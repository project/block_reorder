<?php
/**
 * @file
 * Default theme implementation to configure blocks.
 *
 * Available variables:
 * - $block_regions: An array of regions. Keyed by name with the title as value.
 * - $block_listing: An array of blocks keyed by region and then delta.
 * - $form_submit: Form submit button.
 *
 * Each $block_listing[$region] contains an array of blocks for that region.
 *
 * Each $data in $block_listing[$region] contains:
 * - $data->region_title: Region title for the listed block.
 * - $data->block_title: Block title.
 * - $data->region_select: Drop-down menu for assigning a region.
 * - $data->weight_select: Drop-down menu for setting weights.
 * - $data->configure_link: Block configuration link.
 * - $data->delete_link: For deleting user added blocks.
 *
 * @see template_preprocess_block_admin_display_form()
 * @see theme_block_admin_display()
 *
 * @ingroup themeable
 */
?>
<?php
$regions_to_show = $form['showing_regions']['#value'];
$block_regions = $form['block_regions']['#value'];
$block_array = $form['blocks'];
$block_listing = array();
foreach (element_children($block_array) as $i) :
  $block = $block_array[$i];
  // Fetch the region for the current block.
  $region = (isset($block['region']['#default_value']) ? $block['region']['#default_value'] : BLOCK_REGION_NONE);

  // Set special classes needed for table drag and drop.
  $block['region']['#attributes']['class'] = array('block-region-select', 'block-region-' . $region);
  $block['weight']['#attributes']['class'] = array('block-weight', 'block-weight-' . $region);

  $block_listing[$region][$i] = new stdClass();
  $block_listing[$region][$i]->row_class = !empty($block['#attributes']['class']) ? implode(' ', $block['#attributes']['class']) : '';
  $block_listing[$region][$i]->block_modified = !empty($block['#attributes']['class']) && in_array('block-modified', $block['#attributes']['class']);
  $block_listing[$region][$i]->block_title = drupal_render($block['info']);
  $block_listing[$region][$i]->region_select = drupal_render($block['region']) . drupal_render($block['theme']);
  $block_listing[$region][$i]->weight_select = drupal_render($block['weight']);
  $block_listing[$region][$i]->configure_link = drupal_render($block['configure']);
  $block_listing[$region][$i]->delete_link = !empty($block['delete']) ? drupal_render($block['delete']) : '';
  $block_listing[$region][$i]->printed = FALSE;
endforeach;

foreach ($block_regions as $region => $title) :
  drupal_add_tabledrag('blocks', 'match', 'sibling', 'block-region-select', 'block-region-' . $region, NULL, FALSE);
  drupal_add_tabledrag('blocks', 'order', 'sibling', 'block-weight', 'block-weight-' . $region);
endforeach;
$sticky_class = (!empty($regions_to_show)) ? 'sticky-enabled' : NULL
?>
<div style="margin-bottom: -10px;"><b><?php print t('Note:'); ?></b> <?php print t('Block reorder is possible with in the region Only.'); ?></div>
<table id="blocks" class="sticky-enabled">
  <thead>
    <tr>
      <th><?php print t('Block'); ?></th>
      <th style="display: none;"><?php print t('Region'); ?></th>
      <th style="display: none;"><?php print t('Weight'); ?></th>
      <th colspan="2" style="display: none;"><?php print t('Operations'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php $row = 0; ?>
    <?php
    if (!empty($regions_to_show)) :
      foreach ($block_regions as $region => $title) :
        if (in_array($region, $regions_to_show)) :
          ?>
          <tr class="region-title region-title-<?php print $region ?>">
            <td colspan="5"><?php print $title; ?></td>
          </tr>
          <tr class="region-message region-<?php print $region ?>-message <?php print empty($block_listing[$region]) ? 'region-empty' : 'region-populated'; ?>">
            <td colspan="5"><em><?php print t('No blocks in this region'); ?></em></td>
          </tr>
          <?php
          if (!empty($block_listing[$region])) :
            foreach ($block_listing[$region] as $delta => $data) :
              ?>
              <tr class="draggable <?php print $row % 2 == 0 ? 'odd' : 'even'; ?><?php print $data->row_class ? ' ' . $data->row_class : ''; ?>">
                <td class="block"><?php print $data->block_title; ?></td>
                <td style="display: none;"><?php print $data->region_select; ?></td>
                <td style="display: none;"><?php print $data->weight_select; ?></td>
                <td style="display: none;"><?php print $data->configure_link; ?></td>
                <td style="display: none;"><?php print $data->delete_link; ?></td>
              </tr>
              <?php
              $row++;
            endforeach;
          endif;
        endif;
      endforeach;
    else:
      ?>
      <tr>
        <td><?php print t('* Please select region in field settings. <br /> * Check the content type in block config.'); ?></td>
      </tr>
    <?php
    endif;
    ?>
  </tbody>
</table>
