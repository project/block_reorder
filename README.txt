-- SUMMARY --

It will help to manage the block of default theme at node level.
Note:- You must select the content type in block configuration.
https://drupal.org/files/project-images/block_reorder_content_type_select.png

Note:- You can not change the region of block at template level. 
       Reorder only with in the region.

-- REQUIREMENTS --

None.

-- STEPS TO FOLLOW --
1. Enable the module. After enabling, this will add a widget type.
https://drupal.org/files/project-images/widget_2.png

2. Create a field of widget type Block Reorder in any content type. 
   At field configuration, Select the regions of block in which you want to
   reorder the blocks.

4. Create  node of that content type and manage the order of block at node
   level and see the respective changes.
